#!/usr/bin/env python3

# On import la bibliothèque tkinter qui permet de réaliser une interface graphique
import tkinter as tk

# On crée la variable 'window' qui contient l'objet racine Tk (une fenetre)
window = tk.Tk()

# On donne un titre et une taille par défaut à notre fenetre
window.title("DD destroyer")
window.geometry("800x600")

# On crée la variable 'first_frame' qui contient l'objet tk.Frame (un ensemble de widgets) relié à l'objet `window`
first_frame = tk.Frame(window)

# On crée une fonction nommée `first_screen`
def first_screen():
    # On applique la Frame sur sa fenetre avec la méthode `pack()`
    first_frame.pack()
    
    # On crée la variable label1 dans laquelle on met l'objet tk.Label (un simple texte)
    label1 = tk.Label(first_frame, text="Ceci est un programme qui détruira votre disque dur...Gnihiiihihiihihi")
    # On applique le label sur la Frame `first_frame`
    label1.pack()

# On appelle la fonction `first_screen`
first_screen()

# On appelle `mainloop` qui créé la fenetre et reste à l'écoute des éventuels événements (clic, etc).
window.mainloop()
